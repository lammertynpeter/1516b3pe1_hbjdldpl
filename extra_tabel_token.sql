USE [Spionshop]
GO

ALTER TABLE [dbo].[Tokens] DROP CONSTRAINT [FK_Tokens_Klant]
GO

/****** Object:  Table [dbo].[Tokens]    Script Date: 28/12/2015 17:13:39 ******/
DROP TABLE [dbo].[Tokens]
GO

/****** Object:  Table [dbo].[Tokens]    Script Date: 28/12/2015 17:13:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tokens](
	[TokenId] [int] IDENTITY(1,1) NOT NULL,
	[Klant_id] [smallint] NOT NULL,
	[AuthToken] [nvarchar](250) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
	[ExpiresOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Tokens] PRIMARY KEY CLUSTERED 
(
	[TokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Tokens]  WITH CHECK ADD  CONSTRAINT [FK_Tokens_Klant] FOREIGN KEY([Klant_id])
REFERENCES [dbo].[Klant] ([Klant_id])
GO

ALTER TABLE [dbo].[Tokens] CHECK CONSTRAINT [FK_Tokens_Klant]
GO


