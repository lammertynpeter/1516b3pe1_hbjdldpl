﻿using SpionshopWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing.Imaging;
using System.Drawing;
//using ImageProcessor;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Text.RegularExpressions;

namespace WpfSpionshop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public class BitmapImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                               object parameter, CultureInfo culture)
        {
            try
            {
                BitmapImage imgTemp = new BitmapImage();
                imgTemp.BeginInit();
                imgTemp.CacheOption = BitmapCacheOption.OnLoad;
                imgTemp.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                imgTemp.UriSource = new Uri((string)value);
                imgTemp.EndInit();
                return imgTemp;
            }
            catch 
            {
                BitmapImage imgTemp = new BitmapImage();
                imgTemp.BeginInit();
                imgTemp.CacheOption = BitmapCacheOption.OnLoad;
                imgTemp.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                string basePath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                string localPath = new Uri(basePath).LocalPath;
                string newPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(localPath, @"..\\..\\..\\"));
                string Path = newPath + "SpionShopWebsite\\Content\\Images\\notfound.gif";
                imgTemp.UriSource = new Uri(Path);
                imgTemp.EndInit();
                return imgTemp;
            }
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }



    public partial class MainWindow : Window

    {

        bool foto = false;

        public MainWindow()
        {
            InitializeComponent();



            lbCat.ItemsSource = CategorieDTOS;
            lbArt.ItemsSource = ArtikelDTOS;
            lblCatID.ItemsSource = CatIDS;
            Startup();
     



        }

        private void Startup()
        {
            vulListBox();

        }
 

        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new System.Drawing.Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        private async void vulListBox()
        {
      
            HttpClient client = GetClient();
            var url = "api/Categories";
            var response = await client.GetAsync(url);
            var cats = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer JS = new JavaScriptSerializer();
            List<CategorieDTO> categories = JS.Deserialize<List<CategorieDTO>>(cats);
            CategorieDTOS.Clear();
            ArtikelDTOS.Clear();
            CatIDS.Clear();
            foreach (CategorieDTO cat in categories)
            {
                CategorieDTOS.Add(new CategorieDTO(cat.Categorie1, cat.Cat_id));
                CatIDS.Add(new CatID(cat.Cat_id));

            }
            lbCat.SelectedIndex = 0;
            lbArt.SelectedIndex = 0;

        }

        private ObservableCollection<CategorieDTO> CategorieDTOS = new ObservableCollection<CategorieDTO>();
        private ObservableCollection<ArtikelDTO> ArtikelDTOS = new ObservableCollection<ArtikelDTO>();
        private ObservableCollection<CatID> CatIDS = new ObservableCollection<CatID>();
        public HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:49497/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        private async void btnAddCat_Click(object sender, RoutedEventArgs e)
        {
            if (txtCategorieNaam.Text != null && txtCategorieNaam.Text != "")
            {
                BindingExpression binding = txtCategorieNaam.GetBindingExpression(TextBox.TextProperty);
                binding.UpdateSource();

                var url = "api/Categories/";
                HttpClient client = GetClient();
                string catNaam = "";
                try
                {
                    CategorieDTO CDTO = new CategorieDTO();
                    {
                        CDTO.Categorie1 = txtCategorieNaam.Text;
                    };
                    var res = await client.PostAsJsonAsync(url, CDTO);                    
                    if (res.IsSuccessStatusCode)
                    {
                        vulListBox();
                        string arts = res.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer JS = new JavaScriptSerializer();
                        CategorieDTO c = JS.Deserialize<CategorieDTO>(arts);
                        catNaam = c.Categorie1;
                        lblOpmerking.Content = "De categorie " + catNaam + " werd succesvol toegevoegd.";
                    }
                }
                catch
                {

                }
                
            }
        }

        private async void btnChangeCat_Click(object sender, RoutedEventArgs e)
        {
            if (lbCat.SelectedItem != null)
            {
                string aangepasteNaam = txtCategorieNaam.Text.ToString();
                var value = Convert.ToInt16(txtCatId.Text);
                var url = "api/Categories/" + value;
                HttpClient client = GetClient();
                try
                {
                    CategorieDTO CDTO = new CategorieDTO();
                    {
                        CDTO.Cat_id = value;
                        CDTO.Categorie1 = txtCategorieNaam.Text;
                    };
                    var response = await client.PutAsJsonAsync(url, CDTO);
                    if (response.IsSuccessStatusCode)
                    {
                        lblOpmerking.Content = "De categorie " + txtCategorieNaam.Text + " werd succesvol aangepast.";

                    }
                }
                catch
                {

                }
                (lbCat.SelectedItem as CategorieDTO).Categorie1 = aangepasteNaam;
            }
        }

        private async void btnDeleteCat_Click(object sender, RoutedEventArgs e)
        {
            if (lbCat.SelectedItem != null)
            {
                var value = Convert.ToInt16(txtCatId.Text);
                string naam = txtCategorieNaam.Text;
               
                var url = "api/Categories/" + value;
                HttpClient client = GetClient();
                var response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                   
                    lblOpmerking.Content = "De categorie " + naam + " werd succesvol verwijderd.";
                    CategorieDTOS.Remove(lbCat.SelectedItem as CategorieDTO);
                    foreach(CatID c in CatIDS)
                    {
                        if (c.Cat_id == value)
                        {
                            CatIDS.Remove(c);
                            break;
                        }
                    }
                    

                }
                string test = response.StatusCode.ToString();
                if (test == "Conflict")
                {
                    lblOpmerking.Content = "De categorie " + naam + " kan niet verwijderd worden" + Environment.NewLine + "Verwijder eerst alle artikels";
                }
            }

        }

        private async void lbCat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var url = "api/Artikels/" + txtCategorieNaam.Text;
            if (txtCategorieNaam.Text != "")
            {
                HttpClient client = GetClient();
                UriBuilder baseUri = new UriBuilder("http://localhost:49497/api/artikels?");
                string queryToAppend = "categorienaam=" + txtCategorieNaam.Text;
                if (baseUri.Query != null)
                    baseUri.Query = baseUri.Query.Substring(1) + queryToAppend;
                string uri = baseUri.ToString();
                HttpResponseMessage res = await client.GetAsync(uri);
                string basePath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                string localPath = new Uri(basePath).LocalPath;
                string newPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(localPath, @"..\\..\\..\\"));
                var arts = res.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer JS = new JavaScriptSerializer();
                List<ArtikelDTO> artikels = JS.Deserialize<List<ArtikelDTO>>(arts);



                ArtikelDTOS.Clear();
                foreach (ArtikelDTO art in artikels)
                {
                    string fotoPath = "";
                    fotoPath = newPath + "SpionShopWebsite\\Content\\Images\\Big\\" + Convert.ToString(art.Artikel_id) + ".gif";

                    ArtikelDTOS.Add(new ArtikelDTO(art.Artikel1, art.Artikel_id, art.Cat_id, art.Omschrijving, art.VerkoopPrijs, art.InStock, fotoPath));

                }


            }


        }

        private void lbArt_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        public class CategorieDTO : INotifyPropertyChanged
        {
            public CategorieDTO()
            {

            }

            public CategorieDTO(string categorie1)
            {
                this.Categorie1 = categorie1;
            }
            public CategorieDTO(string categorie1, short cat_id)
            {
                this.Categorie1 = categorie1;
                this.Cat_id = cat_id;
            }

            private string categorie1;
            public string Categorie1
            {
                get { return this.categorie1; }
                set
                {
                    if (this.categorie1 != value)
                    {
                        this.categorie1 = value;
                        this.NotifyPropertyChanged("Categorie1");
                    }
                }
            }
            private short cat_id;
            public short Cat_id
            {
                get { return this.cat_id; }
                set
                {
                    if (this.cat_id != value)
                    {
                        this.cat_id = value;
                        this.NotifyPropertyChanged("Cat_id");
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public class CatID : INotifyPropertyChanged
        {
            public CatID(short id)
            {
                this.Cat_id = id;
            }
            private short cat_id { get; set; }
            public short Cat_id
            {
                get { return this.cat_id; }
                set
                {
                    if (this.cat_id != value)
                    {
                        this.cat_id = value;
                        this.NotifyPropertyChanged("Cat_id");
                    }
                }

            
        }

            public event PropertyChangedEventHandler PropertyChanged;
            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public class ArtikelDTO : INotifyPropertyChanged,IDataErrorInfo
        {
            public event PropertyChangedEventHandler PropertyChanged;
            private short artikel_id { get; set; }
            private short cat_id { get; set; }
            private string artikel1 { get; set; }
            private string omschrijving { get; set; }
            //private decimal verkoopPrijs { get; set; }
            private string verkoopPrijs { get; set; }
            //private short inStock { get; set; }
            private string inStock { get; set; }
            private string fotoPath { get; set; }

            public ArtikelDTO()
            {

            }

            public ArtikelDTO(string fotoPath)
            {
                this.FotoPath = fotoPath;
            }

            public ArtikelDTO(string Artikel1, short Artikel_id, short Cat_id, string Omschrijving, string vp, string instock, string fotoPath)
            {
                this.artikel1 = Artikel1;
                this.artikel_id = Artikel_id;
                this.cat_id = Cat_id;
                this.omschrijving = Omschrijving;
                verkoopPrijs = vp;
                this.inStock = instock;
                this.fotoPath = fotoPath;
            }



            public short Artikel_id
            {
                get { return this.artikel_id; }
                set
                {
                    if (this.artikel_id != value)
                    {
                        this.artikel_id = value;
                        this.NotifyPropertyChanged("Artikel_id");
                    }
                }
            }

            public short Cat_id
            {
                get { return this.cat_id; }
                set
                {
                    if (this.cat_id != value)
                    {
                        this.cat_id = value;
                        this.NotifyPropertyChanged("Cat_id");
                    }
                }
            }

            public string Artikel1
            {
                get { return this.artikel1; }
                set
                {
                    if (this.artikel1 != value)
                    {
                        this.artikel1 = value;
                        this.NotifyPropertyChanged("Artikel1");
                    }
                }
            }

            public string Omschrijving
            {
                get { return this.omschrijving; }
                set
                {
                    if (this.omschrijving != value)
                    {
                        this.omschrijving = value;
                        this.NotifyPropertyChanged("Omschrijving");
                    }
                }
            }
 

            public string FotoPath
            {
                get { return this.fotoPath; }
                set
                {
                    if (this.fotoPath != value)
                    {
                        this.fotoPath = value;
                        this.NotifyPropertyChanged("FotoPath");

                    }
                }
            }

            public string VerkoopPrijs
            {
                get { return this.verkoopPrijs; }
                set
                {
                    if (this.verkoopPrijs != value)
                    {
                        this.verkoopPrijs = value;
                        this.NotifyPropertyChanged("VerkoopPrijs");
                    }
                }
            }
 

            public string InStock
            {
                get { return this.inStock; }
                set
                {
                    if (this.inStock != value)
                    {
                        this.inStock = value;
                        this.NotifyPropertyChanged("InStock");
                    }
                }
            }

            public string Error
            {
                get
                {
                    return null;
                }
            }

            public string this[string columnName]
            {
                get
              {
                   bool OK = true;
                    bool neg = false;
                    bool pos = false;
                    if(columnName== "VerkoopPrijs")
                    {
                        decimal dummy;
                        NumberStyles style;
                        style = NumberStyles.Any;
                        CultureInfo culture;
                        culture = CultureInfo.InvariantCulture;
                        var test = verkoopPrijs.ToString();
                     
                        Decimal.TryParse(test,style,culture, out dummy);
                    
                        if (dummy == 0)
                        {


                            OK = false;
                        }
                        if (dummy < 0)
                        {
                            neg = true;
                        }
                       

                    }
                    else if(columnName == "InStock")
                    {
                        short dummy;
                        var stock = inStock.ToString();
                        short.TryParse(InStock.ToString(),out dummy);
                        if (dummy == 0 ||dummy<=0 )
                        {
                            
                            if (stock == ""||stock=="0")
                            {
                                OK = true;
                            }
                            else
                            {
                                if (dummy < 0)
                                {
                                    neg = true;
                                }
                                else if (Convert.ToInt32(stock) >= 32767)
                                {
                                    pos = true;
                                }
                                else
                                {
                                    OK = false;
                                }
                              
                            }

                        }


                    }

                    if (!OK)
                    {
                        return "Gelieve enkel numerieke waarden in te voeren";
                    }
                    if (neg)
                    {
                        return "Gelieve een positiefgetal in te voeren";
                    }
                    if (pos)
                    {
                        return "Max waarde is 32767.";
                    }
                    return null;
                }
                
            }

            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }


        }



 

        private async void btnChangeArt_Click(object sender, RoutedEventArgs e)
        {
            if (lbArt.SelectedItem != null)
            {
                bool veranderCategorie = false;
                string aangepasteNaam = txtArtNaam.Text.ToString();
                var value = Convert.ToInt16(txtArtID.Content);
                var url = "api/Artikels/" + value;
               
                HttpClient client = GetClient();
                try
                {
                    if (foto)
                    {
                        PasFotoAan(value);
                        foto = false;
                    }
                    foreach(ArtikelDTO a in ArtikelDTOS)
                    {
                        if(a.Artikel_id== Convert.ToInt16(txtArtID.Content))
                        {
                            a.Artikel1 = txtArtNaam.Text;
                      if (lblCatID.SelectedItem != null)
                        {
                            CatID c = (lblCatID.SelectedItem as CatID);
                                if(a.Cat_id != c.Cat_id)
                                {
                                    veranderCategorie= true;
                                }
                            a.Cat_id = c.Cat_id;
                        }
                        else
                        {
                            a.Cat_id = Convert.ToInt16(txtCatID.Text);
                        }
                        a.Omschrijving = txtOmsch.Text;
                            
                           // a.VerkoopPrijs = decimal.Parse(txtVerkoop.Text,CultureInfo.InvariantCulture);
                            a.VerkoopPrijs = Convert.ToString(txtVerkoop.Text, CultureInfo.InvariantCulture);
                            a.InStock = Convert.ToString(txtStock.Text);
                            if (!foto)
                                a.FotoPath = txtFotoPath.Text;
                            break;
                        }
                    }
                    ArtikelDetailDTO ADTO = new ArtikelDetailDTO();
                    {
                        ADTO.Artikel1 = txtArtNaam.Text;

                    
                    if (lblCatID.SelectedItem != null)
                    {
                        CatID c = (lblCatID.SelectedItem as CatID);
                        ADTO.Cat_id = c.Cat_id;
                    }
                    else
                    {
                        ADTO.Cat_id = Convert.ToInt16(txtCatID.Text);
                    }
                    ADTO.Omschrijving = txtOmsch.Text;                        
                        ADTO.Verkoopprijs = Convert.ToDecimal(txtVerkoop.Text, CultureInfo.InvariantCulture);                        
                        ADTO.Instock = Convert.ToInt16(txtStock.Text);
                       ADTO.Artikel_id = Convert.ToInt16(txtArtID.Content);                  
                    };
                    var response = await client.PutAsJsonAsync(url, ADTO);
                    if (response.IsSuccessStatusCode)
                    {
                        if (veranderCategorie)
                        {

                            foreach(ArtikelDTO a in ArtikelDTOS)
                            {
                                CatID c = (lblCatID.SelectedItem as CatID);
                                if (a.Artikel_id== Convert.ToInt16(txtArtID.Content))
                                {
                                    ArtikelDTOS.Remove(a);
                                    break;
                                }
                            }
                            
                        }
                        lblOpmerking.Content = "Het artikel " + txtArtNaam.Text + " werd succesvol aangepast.";

                    }
            }
                catch
            {

            }
                if (!veranderCategorie)
                {
                    (lbArt.SelectedItem as ArtikelDTO).Artikel1 = aangepasteNaam;
                }
               
            }
        }

        private void PasFotoAan(short ID)
        {
            string basePath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            string localPath = new Uri(basePath).LocalPath;
            string newPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(localPath, @"..\\..\\..\\"));
            string tijdelijkPath = newPath + "SpionShopWebsite\\Content\\Images\\Big\\Oud\\tijdelijk.gif";
            System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(tijdelijkPath);
            Bitmap image = ResizeImage(imgPhoto, 185, 309);
            File.Delete(newPath + "SpionShopWebsite\\Content\\Images\\Big\\" + Convert.ToString(ID) + ".gif");
            image.Save(newPath + "SpionShopWebsite\\Content\\Images\\Big\\" + Convert.ToString(ID) + ".gif", System.Drawing.Imaging.ImageFormat.Gif);
            txtFotoPath.Text = newPath + "SpionShopWebsite\\Content\\Images\\Big\\" + Convert.ToString(ID) + ".gif";
            newPath += "SpionShopWebsite\\Content\\Images\\Thumbs\\" + Convert.ToString(ID) + ".gif";
            image = ResizeImage(imgPhoto, 100, 75);
            File.Delete(newPath);
            image.Save(newPath, System.Drawing.Imaging.ImageFormat.Gif);
            image.Dispose();
            imgPhoto.Dispose();
            File.Delete(tijdelijkPath);
        }

        private async void btnDeleteArt_Click(object sender, RoutedEventArgs e)
        {
            if (lbArt.SelectedItem != null)
            {
                var value = Convert.ToInt16(txtArtID.Content);
                string naam = txtArtNaam.Text;
                
                 var url = "api/Artikels/" + value;
                HttpClient client = GetClient();
                var response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    ArtikelDTOS.Remove(lbArt.SelectedItem as ArtikelDTO);
                    lblOpmerking.Content = "Het Artikel " + naam + " werd succesvol verwijderd.";
                    

                }
                string test = response.StatusCode.ToString();
                if (test == "Conflict")
                {
                    lblOpmerking.Content = "Het artikel " + naam + " kan niet verwijderd worden." + Environment.NewLine + "Het werd minstens eenmaal besteld.";
                }
            }

        }

        private async void btnAddArt_Click(object sender, RoutedEventArgs e)
        {
            string NieuweNaam = txtArtNaam.Text.ToString();
            var url = "api/Artikels/";
            HttpClient client = GetClient();
            ArtikelDTO ArDTO = new ArtikelDTO();
            try
            {               
                ArtikelDetailDTO ADTO = new ArtikelDetailDTO();
                {
                    ADTO.Artikel1 = txtArtNaam.Text;
                    ArDTO.Artikel1= txtArtNaam.Text;
                    if (lblCatID.SelectedItem != null)
                    {
                        CatID c = (lblCatID.SelectedItem as CatID);
                        ArDTO.Cat_id = c.Cat_id;
                        ADTO.Cat_id = c.Cat_id;
                    }
                    else
                    {
                        ADTO.Cat_id = Convert.ToInt16(txtCatID.Text);
                        ArDTO.Cat_id = Convert.ToInt16(txtCatID.Text);
                    }
                    ADTO.Omschrijving = txtOmsch.Text;
                    ArDTO.Omschrijving = txtOmsch.Text;
                    ADTO.Verkoopprijs = Math.Round(decimal.Parse(txtVerkoop.Text, new CultureInfo("en-US")),3);
                    ArDTO.VerkoopPrijs= Convert.ToString(ADTO.Verkoopprijs);
                    ADTO.Instock = Convert.ToInt16(txtStock.Text);
                    ArDTO.InStock= Convert.ToString(txtStock.Text);
                    if (!foto)
                        ArDTO.FotoPath = txtFotoPath.Text;
                };
                var response = await client.PostAsJsonAsync(url, ADTO);
                short X;
                if (response.IsSuccessStatusCode)
                {
                    var value = response.Headers;
                    string nummer = value.Location.AbsoluteUri;
                    X= Convert.ToInt16(nummer.Substring(nummer.LastIndexOf("/") + 1));
                    lblOpmerking.Content = "Het artikel " + txtArtNaam.Text + " werd succesvol toegevoegd.";
                
                if (foto)
                {
                    PasFotoAan(X);
                     ArDTO.FotoPath= txtFotoPath.Text;
                    foto = false;
                }
                ArDTO.Artikel_id = X;
                ArtikelDTOS.Add(ArDTO);
                }
            }
            catch
            {
            }
            vulListBox();             
            if(lbArt.SelectedItem!=null)
                (lbArt.SelectedItem as ArtikelDTO).Artikel1 = NieuweNaam;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".gif";
            dlg.Filter = "Images (.img)|*.img|Images (.gif)|*.gif |Images(.jpg)|*.jpg |All ( . ) |*.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                string filenameUitlezen = filename.Replace("\\", "/");
                txtFotoPath.Text = filenameUitlezen;
                string basePath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                var fotobron = txtFotoPath.Text;
                fotobron = fotobron.Remove(0, 8);
                fotobron = fotobron.Replace("/", "\\");
                string localPath = new Uri(basePath).LocalPath;
                string newPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(localPath, @"..\\..\\..\\"));
                newPath += "SpionShopWebsite\\Content\\Images\\Big\\Oud\\tijdelijk.gif";
                if (newPath.ToUpper() != filename.ToUpper())
                {
                    System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(filename);
                    Bitmap image = new Bitmap(imgPhoto);;
                    image.Save(newPath, System.Drawing.Imaging.ImageFormat.Gif);
                    foreach (ArtikelDTO a in ArtikelDTOS)
                    {
                        if (a.Artikel_id == Convert.ToInt16(txtArtID.Content))
                        {
                            a.FotoPath = filename;
                            break;
                        }
                    }
                    foto = true;

                }
            }
        }
    }
}
    


