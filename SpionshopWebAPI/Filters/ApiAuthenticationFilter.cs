﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using SpionshopWebAPI.Services;

namespace SpionshopWebAPI.Filters
{
    public class ApiAuthenticationFilter : GenericAuthenticationFilter
    {
        public ApiAuthenticationFilter()
        {
        }

        public ApiAuthenticationFilter(bool isActive) : base(isActive)
        {
        }

        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            //var provider = actionContext.ControllerContext.Configuration
            var provider = new UserServices();
            //     .DependencyResolver.GetService(typeof(IUserServices)) as IUserServices;
            //if (provider != null)
            //{
            //var userId = provider.Authenticate(username, password);

            var userId = provider.Authenticate(username, password);
                if (userId > 0)
                {
                    var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                    {
                        basicAuthenticationIdentity.Klant_id = userId;
                    }
                    return true;
                }
            //}
            return false;
        }
    }
}