﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace SpionshopWebAPI.Filters
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public string Pwd { get; set; }
        public string Gebruikersnaam { get; set; }
        public short Klant_id { get; set; }
        
        public BasicAuthenticationIdentity(string userName, string password) : base(userName, "Basic")
        {
            Pwd = password;
            Gebruikersnaam = userName;
        }
    }
}