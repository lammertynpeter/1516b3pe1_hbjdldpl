﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpionshopWebAPI.Services
{
    public interface IUserServices
    {
        short Authenticate(string userName, string password);
    }
}
