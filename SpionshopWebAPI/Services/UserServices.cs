﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopLib;
using SpionshopWebAPI.Models;

namespace SpionshopWebAPI.Services
{
    public class UserServices : IUserServices
    {
        private SpionshopEntities db = new SpionshopEntities();

        public UserServices()
        {

        }

        public short Authenticate(string userName, string password)
        {
            //Blijkbaar werkt de centrale Mapper.Create via App_Start enkel rechtstreeks op de api en niet vanuit mvc
            //Nog niet kunnen uitzoeken waarom, maar voorlopig dus nog steeds apart
            Mapper.CreateMap<Klant, KlantDTO>();
            var klant = db.Klant
                .ProjectTo<KlantDTO>().SingleOrDefault(k => k.Gebruikersnaam == userName && k.Pwd == password);
            if (klant != null && klant.Klant_id > 0)
            {
                return klant.Klant_id;
            }

            return 0;
        }
    }
}