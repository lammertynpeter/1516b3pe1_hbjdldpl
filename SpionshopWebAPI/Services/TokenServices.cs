﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopLib;
using SpionshopWebAPI.Models.Proxies;

namespace SpionshopWebAPI.Services
{
    public class TokenServices : ITokenServices
    {
        private SpionshopEntities db = new SpionshopEntities();

        #region Private member variables.
        //private readonly UnitOfWork _unitOfWork;
        #endregion

        #region Public constructor.
        /// <summary>
        /// Public constructor.
        /// </summary>
        //public TokenServices(UnitOfWork unitOfWork)
        public TokenServices()
        {
            //_unitOfWork = unitOfWork;
        }
        #endregion


        #region Public member methods.

        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TokenDTO GenerateToken(short userId)
        {
            //Mapper.CreateMap<Token, TokenDTO>();
            string token = Guid.NewGuid().ToString();
            DateTime issuedOn = DateTime.Now;
            DateTime expiredOn = DateTime.Now.AddSeconds(
            Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
            var tokendomain = new Token
            {
                Klant_id = userId,
                AuthToken = token,
                IssuedOn = issuedOn,
                ExpiresOn = expiredOn
            };

            //_unitOfWork.TokenRepository.Insert(tokendomain);
            db.Tokens.Add(tokendomain);
            //_unitOfWork.Save();
            db.SaveChanges();
            //var tokenModel = new TokenDTO()
            //{
            //    Klant_id = userId,
            //    IssuedOn = issuedOn,
            //    ExpiresOn = expiredOn,
            //    AuthToken = token
            //};
            Mapper.CreateMap<Token, TokenDTO>();
            var tokenModel = Mapper.Map<TokenDTO>(tokendomain);

            return tokenModel;
        }

        /// <summary>
        /// Method to validate token against expiry and existence in database.
        /// </summary>
        /// <param name="tokenGuid"></param>
        /// <returns></returns>
        public bool ValidateToken(string tokenGuid)
        {
            var token = db.Tokens.FirstOrDefault(t => t.AuthToken == tokenGuid && t.ExpiresOn > DateTime.Now);
            if (token != null && !(DateTime.Now > token.ExpiresOn))
            {
                token.ExpiresOn = token.ExpiresOn.AddSeconds(
                Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
                db.Entry(token).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenGuid">true for successful delete</param>
        public bool Kill(string tokenGuid)
        {
            Token token = db.Tokens.FirstOrDefault(x => x.AuthToken == tokenGuid);
            db.Tokens.Remove(token);
            db.SaveChanges();
            var isNotDeleted = db.Tokens.Where(x => x.AuthToken == tokenGuid).Any();
            if (isNotDeleted) { return false; }
            return true;
        }

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true for successful delete</returns>
        public bool DeleteByUserId(short userId)
        {
            IQueryable<Token> tokens = db.Tokens.Where(x => x.Klant_id == userId).AsQueryable();
            foreach(Token t in tokens)
            {
                db.Tokens.Remove(t);
            }
            //db.SaveChanges();
            var isNotDeleted = db.Tokens.Where(x => x.Klant_id == userId).Any();
            return !isNotDeleted;
        }

        #endregion
    }
}