﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpionshopWebAPI.Models.Proxies
{
    public class TokenDTO
    {
        public int TokenId { get; set; }
        public short Klant_id { get; set; }
        public string AuthToken { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}