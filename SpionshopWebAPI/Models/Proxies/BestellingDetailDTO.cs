﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SpionshopWebAPI.Models.Proxies
{
    public class BestellingDetailDTO
    {
        public short Aantal { get; set; }
        public string ArtikelArtikel1 { get; set; }
        public short Artikel_id { get; set; }
        public int B_id { get; set; }
        public int BD_id { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? TotaalPrijs { get; set;}

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? ArtikelVerkoopprijs { get; set; }
        public string KarId { get; set; }
        public string RecordId { get; set; }
    }
}