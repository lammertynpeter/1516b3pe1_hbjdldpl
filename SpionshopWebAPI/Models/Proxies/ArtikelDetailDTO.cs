﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpionshopWebAPI.Models
{
    public class ArtikelDetailDTO
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string CategorieCategorie1 { get; set; }
        public string Omschrijving { get; set; }
        public string Artikel1 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal? Verkoopprijs { get; set; }
        public short? Instock { get; set; }
    }
}
