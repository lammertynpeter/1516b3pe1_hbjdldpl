﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpionshopWebAPI.Models.Proxies
{
	public class BestellingDTO
	{
        public int B_id { get; set; }
        public short Klant_id { get; set; }
        public string KlantNaam { get; set; }
        public DateTime Datum { get; set; }

    }
}