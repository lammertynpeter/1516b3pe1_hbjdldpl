﻿// starturi's voor Web API Services
var bestellingsUri = '/api/Bestellings/';
//var bestellingDetailsUri = '/api/BestellingDetails/';
var categoriesUri = '/api/Categories/';
//var artikelsUri = '/api/Artikels/';

var CategoriesViewModel = function () {

    var self = this;
    self.error = ko.observable();
    self.categorieën = ko.observableArray();


//    self.isEdit = ko.observable(true);
//    self.actionDescription = ko.computed(
//        function () {
//            if (self.isEdit()) return 'Edit categorie';
//            else return 'Add categorie';
//        }
//        );


    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }


    // Haal de basisgegevens van de boeken: lijst van BookDTO's in de Array books
    self.getAllCategories = function () {
        self.ajaxHelper(categoriesUri, 'GET').done(function (data) {
            self.categorieën(data);
        });
    }

    // initiele data ophalen
    self.getAllCategories();

};
ko.applyBindings(new CategoriesViewModel());
var BestellingenViewModel = function () {

    var self = this;
    self.error = ko.observable();
    self.bestellingen = ko.observableArray(); //nodig? 1 bestelling?

    self.Bestelling = function(b_id, klant_id, klantnaam)
    {
        this.B_id = ko.observable(b_id),
        this.Klant_id = ko.observable(klant_id),
        this.Klantnaam = ko.observable(authorname)
    }


    self.isEdit = ko.observable(true);
    self.actionDescription = ko.computed(
        function () {
            if (self.isEdit()) return 'Edit categorie';
            else return 'Add categorie';
        }
        );


    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }

    self.getAllBestellingen = function () {
    alert('test')
        self.ajaxHelper(bestellingsUri, 'GET').done(function (data) {
            self.bestellingen(data);
        });
    }

    // initiele data ophalen
    self.getAllBestellingen();

};

//var BestellingDetailsViewModel = function () {

//    var self = this;
//    self.error = ko.observable();
//    self.bestellingDetails = ko.observableArray();

//    self.ajaxHelper = function (uri, method, data) {
//        self.error(''); // Foutmelding leegmaken
//        return $.ajax({
//            type: method,
//            url: uri,
//            dataType: 'json',
//            contentType: 'application/json',
//            data: data ? JSON.stringify(data) : null
//        }).error(function (jqXHR, textStatus, errorThrown) {
//            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
//        });
//    }


//    // Haal de basisgegevens van de bestellingdetails: lijst van bestellingDetailsDTO's in de Array bestellingDetails
//    self.getAllBestellingDetails = function () {
//        self.ajaxHelper(categoriesUri, 'GET').done(function (data) {
//            self.bestellingDetails(data);
//        });
//    }

//    // initiele data ophalen
//    self.getAllBestellingDetails();

//};

// ViewModel koppelen aan de View
//ko.applyBindings(new CategoriesViewModel());
ko.applyBindings(new BestellingenViewModel());
//ko.applyBindings(new BestellingDetailsViewModel());