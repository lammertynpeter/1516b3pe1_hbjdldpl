﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using SpionshopLib;
using SpionshopWebAPI.Services;
using SpionshopWebAPI.Filters;
using System.Threading;

namespace SpionshopWebAPI.Controllers
{
    [ApiAuthenticationFilter]
    public class AuthenticateController : ApiController
    {
        #region Private variable.

        private readonly ITokenServices _tokenServices;
        //private SpionshopEntities db = new SpionshopEntities();

        #endregion

        #region Public Constructor
        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public AuthenticateController()
        {
            _tokenServices = new TokenServices();
        }
        #endregion

        /// <summary>
        /// Authenticates user and returns token with expiry
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Authenticate()
        {
            if(Thread.CurrentPrincipal!=null && Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                if(basicAuthenticationIdentity != null)
                {
                    var userId = basicAuthenticationIdentity.Klant_id;
                    return GetAuthToken(userId);
                }
            }
            return null;
        }

        /// <summary>
        /// Returns auth token for the validated user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private HttpResponseMessage GetAuthToken(short userId)
        {
            var token = _tokenServices.GenerateToken(userId);
            var response = Request.CreateResponse(HttpStatusCode.OK, "Authorized");
            response.Headers.Add("Token", token.AuthToken);
            response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
            response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
            return response;
        }
    }
}
