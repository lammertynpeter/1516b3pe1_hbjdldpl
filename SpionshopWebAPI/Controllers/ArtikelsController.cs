﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using SpionshopWebAPI.Models;
using SpionshopWebAPI.Models.Proxies;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopWebAPI.Filters;
using SpionshopWebAPI.ActionFilters;

namespace SpionshopWebAPI.Controllers
{
    //[ApiAuthenticationFilter]
    public class ArtikelsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Artikels
        public IQueryable<ArtikelDetailDTO> GetArtikel()
        {
            Mapper.CreateMap<Artikel, ArtikelDetailDTO>();
            var Artikels = db.Artikel.ProjectTo<ArtikelDetailDTO>();
           
            return Artikels;

            
        }



        // GET: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        //Met deze AuthenticationFilter op true of false te zetten geef je aan of er voor gebruik van deze controller authenticatie nodig is of niet
        //Dit kan ook op niveau van een volledige controller gebeuren: [ApiAuthenticationFilter] plaatsen
        // Of op niveau van de volledig api: toevoegen in web.config : GlobalConfiguration.Configuration.Filters.Add(new ApiAuthenticationFilter());
        //[ApiAuthenticationFilter(true)]
        // Om met de Tokens te authenticeren gebruik je AuthorizationRequired filter
        //[AuthorizationRequired]
        public async Task<IHttpActionResult> GetArtikel(short id)
        {

            Mapper.CreateMap<Artikel, ArtikelDetailDTO>();
            var artikel = await db.Artikel
                .ProjectTo<ArtikelDetailDTO>().SingleOrDefaultAsync(b => b.Artikel_id == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        // GET: api/Artikels/categorienaam
       [ResponseType(typeof(Artikel))]
        
        public async Task<IHttpActionResult> GetArtikel(string categorienaam)
        {

            Mapper.CreateMap<Artikel, ArtikelDetailDTO>();
            var artikel = db.Artikel
                        .Include("Categorie")
                        .Where(ac => ac.Categorie.Categorie1 == categorienaam)
                        .ProjectTo<ArtikelDetailDTO>();
                        //.SingleOrDefaultAsync(ac => ac.Cat_id == cat_ID);

            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        public async Task<IHttpActionResult> GetEenArtikel(string naam)
        {
            Mapper.CreateMap<Artikel, ArtikelDetailDTO>();
            var artikel = await db.Artikel
                         .ProjectTo<ArtikelDetailDTO>()
                         .SingleOrDefaultAsync(b => b.Artikel1 == naam);
            
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }


        // PUT: api/Artikels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikel(short id, ArtikelDetailDTO artikelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikelDTO.Artikel_id)
            {
                return BadRequest();
            }

            Mapper.CreateMap<ArtikelDetailDTO, Artikel>();
            Artikel artikel = Mapper.Map<Artikel>(artikelDTO);
           

            db.Entry(artikel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (DbUpdateException ex)
            {
                
            }

            return StatusCode(HttpStatusCode.NoContent);
        }





        // POST: api/Artikels
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> PostArtikel(ArtikelDetailDTO artikelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.CreateMap<ArtikelDetailDTO, Artikel>();
            Artikel artikel = Mapper.Map<Artikel>(artikelDTO);

            db.Artikel.Add(artikel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = artikel.Cat_id }, artikel);
        }

        // DELETE: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> DeleteArtikel(short id)
        {
            Artikel artikel = await db.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            if (await db.BestellingDetail.AnyAsync(c => c.Artikel_id== id))
            {
                return StatusCode(HttpStatusCode.Conflict);
            }

            else
            { 
                db.Artikel.Remove(artikel);
                await db.SaveChangesAsync();
            }

            return Ok(artikel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelExists(short id)
        {
            return db.Artikel.Count(e => e.Artikel_id == id) > 0;
        }
    }
}