﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopWebAPI.Models.Proxies;

namespace SpionshopWebAPI.Controllers
{

    public class BestellingsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Bestellings
        public IQueryable<BestellingDTO> GetBestelling()
        {
            Mapper.CreateMap<Bestelling, BestellingDTO>();
            var Bestellingen = db.Bestelling.ProjectTo<BestellingDTO>();
            return Bestellingen;

        }

        // GET: api/Bestellings/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> GetBestelling(int id)
        {

            Mapper.CreateMap<Bestelling, BestellingDTO>();
            var bestelling = await db.Bestelling
                .ProjectTo<BestellingDTO>().SingleOrDefaultAsync(b => b.B_id == id);
            if (bestelling == null)
            {
                return NotFound();
            }

            return Ok(bestelling);
        }

        // PUT: api/Bestellings/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestelling(int id, BestellingDTO bestellingDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (id != bestellingDTO.B_id)
            //{
            //    return BadRequest();
            //}

            //db.Entry(bestellingDTO).State = EntityState.Modified;

            //try
            //{
                Mapper.CreateMap<BestellingDTO, Bestelling>();
                Bestelling bestelling = Mapper.Map<Bestelling>(bestellingDTO);
                db.Set<Bestelling>().Attach(bestelling); // voor update
                db.Entry(bestelling).State = EntityState.Modified;
                await db.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!BestellingExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}
            return Ok(bestellingDTO);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bestellings
        [ResponseType(typeof(BestellingDTO))]
        public async Task<IHttpActionResult> PostBestelling(BestellingDTO bestellingDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Mapper.CreateMap<BestellingDTO, Bestelling>();
            Bestelling bestelling = Mapper.Map<Bestelling>(bestellingDTO);
            db.Bestelling.Add(bestelling);
            try
            {
                await db.SaveChangesAsync();
                db.Entry(bestelling).Reload();
                bestellingDTO.B_id = bestelling.B_id;
                return CreatedAtRoute("DefaultApi", new { id = bestellingDTO.B_id }, bestellingDTO);
            }
            catch(Exception e)
            {
                return BadRequest();
            }

        }

        //// POST: api/Bestellings
        //[ResponseType(typeof(BestellingDTO))]
        //public async Task<BestellingDTO> PostBestelling(BestellingDTO bestellingDTO)
        //{
        //    //if (!ModelState.IsValid)
        //    //{
        //    //    return BadRequest(ModelState);
        //    //}
        //    Mapper.CreateMap<BestellingDTO, Bestelling>();
        //    Bestelling bestelling = Mapper.Map<Bestelling>(bestellingDTO);
        //    db.Bestelling.Add(bestelling);
        //    await db.SaveChangesAsync();
        //    db.Entry(bestelling).Reload();
        //    bestellingDTO.B_id = bestelling.B_id;

        //    return bestellingDTO;
        //}

        // DELETE: api/Bestellings/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> DeleteBestelling(int id)
        {
            Bestelling bestelling = await db.Bestelling.FindAsync(id);

            if (bestelling == null)
            {
                return NotFound();
            }

            if (await db.BestellingDetail.AnyAsync(c => c.B_id == id))
            {
                return StatusCode(HttpStatusCode.Conflict);
            }
            else
            {
                db.Bestelling.Remove(bestelling);
                await db.SaveChangesAsync();

                return Ok(bestelling);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingExists(int id)
        {
            return db.Bestelling.Count(e => e.B_id == id) > 0;
        }
    }
}