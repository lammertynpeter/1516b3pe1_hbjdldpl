﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopWebAPI.Models.Proxies;

namespace SpionshopWebAPI.Controllers
{
    public class BestellingDetailsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/BestellingDetails/
        //public IQueryable<BestellingDetailDTO> GetBestellingDetail()
        //{
        //    Mapper.CreateMap<BestellingDetail, BestellingDetailDTO>();
        //    var BestellingDetail = db.BestellingDetail.ProjectTo<BestellingDetailDTO>();
        //    return BestellingDetail;
        //}

        // GET: api/BestellingDetails
        public IQueryable<BestellingDetailDTO> GetBestellingDetail(int B_id)
        {
            Mapper.CreateMap<BestellingDetail, BestellingDetailDTO>();
            var BestellingDetail = db.BestellingDetail.ProjectTo<BestellingDetailDTO>().Where(b => b.B_id == B_id);
            return BestellingDetail;
        }

        // GET: api/BestellingDetails/5
        //[ResponseType(typeof(BestellingDetail))]
        //public async Task<IHttpActionResult> GetBestellingDetail(int id)
        //{
        //    Mapper.CreateMap<BestellingDetail, BestellingDetailDTO>();
        //    var bestellingDetail = await db.Bestelling
        //        .ProjectTo<BestellingDetailDTO>().SingleOrDefaultAsync(b => b.B_id == id);
        //    return Ok(bestellingDetail);
        //}

        // PUT: api/BestellingDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestellingDetail(int id, BestellingDetailDTO bestellingDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (id != bestellingDetail.BD_id)
            //{
            //    return BadRequest();
            //}

            //db.Entry(bestellingDetail).State = EntityState.Modified;

            //try
            //{
            Mapper.CreateMap<BestellingDetailDTO, BestellingDetail>();
            BestellingDetail bestellingDetail = Mapper.Map<BestellingDetail>(bestellingDetailDTO);
            db.Set<BestellingDetail>().Attach(bestellingDetail); // voor update
            db.Entry(bestellingDetail).State = EntityState.Modified;
            await db.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!BestellingDetailExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}
            return Ok(bestellingDetailDTO);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/BestellingDetails
        //[ResponseType(typeof(BestellingDetailDTO))]
        //public async Task<IHttpActionResult> PostBestellingDetail(BestellingDetailDTO bestellingDetailDTO)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Mapper.CreateMap<BestellingDetailDTO, BestellingDetail>();
        //    BestellingDetail bestellingDetail = Mapper.Map<BestellingDetail>(bestellingDetailDTO);
        //    db.BestellingDetail.Add(bestellingDetail);
        //    await db.SaveChangesAsync();

        //    return CreatedAtRoute("DefaultApi", new { id = bestellingDetailDTO.BD_id }, bestellingDetailDTO);
        //}

        // POST: api/BestellingDetails
        [ResponseType(typeof(List<BestellingDetailDTO>))]
        public async Task<IHttpActionResult> PostBestellingDetail(List<BestellingDetailDTO> bestellingDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            foreach(BestellingDetailDTO bd in bestellingDetailDTO)
            {
                Mapper.CreateMap<BestellingDetailDTO, BestellingDetail>();
                BestellingDetail bestellingDetail = Mapper.Map<BestellingDetail>(bd);
                try
                {
                    
                    db.BestellingDetail.Add(bestellingDetail);
                    await db.SaveChangesAsync();
                }
                catch(Exception e)
                {
                    db.BestellingDetail.RemoveRange(db.BestellingDetail.Where(x => x.B_id == bd.B_id));
                    db.Bestelling.RemoveRange(db.Bestelling.Where(x => x.B_id == bd.B_id));
                    await db.SaveChangesAsync();
                    return BadRequest();
                }



            }

            return CreatedAtRoute("DefaultApi", new { id = 0}, bestellingDetailDTO);
        }

        // DELETE: api/BestellingDetails/5
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> DeleteBestellingDetail(int id)
        {
            BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
            if (bestellingDetail == null)
            {
                return NotFound();
            }

            db.BestellingDetail.Remove(bestellingDetail);
            await db.SaveChangesAsync();

            return Ok(bestellingDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingDetailExists(int id)
        {
            return db.BestellingDetail.Count(e => e.BD_id == id) > 0;
        }
    }
}