﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using SpionshopWebAPI.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopWebAPI.Filters;

namespace SpionshopWebAPI.Controllers
{
    public class KlantsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Klants
        public IQueryable<KlantDTO> GetKlant()
        {
            Mapper.CreateMap<Klant, KlantDTO>();
            var klanten = db.Klant.ProjectTo<KlantDTO>();
            return klanten;
        }

        // GET: api/Klants/5
        [ResponseType(typeof(Klant))]
        //[ApiAuthenticationFilter(true)]
        [Route("api/Klants/{id:int}")]
        public async Task<IHttpActionResult> GetKlant(short id)
        {
            Mapper.CreateMap<Klant, KlantDTO>();
            var klant = await db.Klant
                .ProjectTo<KlantDTO>().SingleOrDefaultAsync(b => b.Klant_id == id);
            if (klant == null)
            {
                return NotFound();
            }

            return Ok(klant);

        }

        // GET : api/Klants/O
        [ResponseType(typeof (Klant))]
        [Route("api/Klants/{userName}")]
        public async Task<IHttpActionResult> GetKlant(string userName)
        {
            Mapper.CreateMap<Klant, KlantDTO>();
            var klant = await db.Klant
                .ProjectTo<KlantDTO>().SingleOrDefaultAsync(k => k.Gebruikersnaam == userName);
            if (klant == null)
            {
                return NotFound();
            }
            return Ok(klant);
        }

        // PUT: api/Klants/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutKlant(short id, KlantDTO klantDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.CreateMap<KlantDTO, Klant>();
            Klant klant = Mapper.Map<Klant>(klantDTO);
            db.Set<Klant>().Attach(klant);
            db.Entry(klant).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Ok(klantDTO);
        }

        // POST: api/Klants
        [ResponseType(typeof(KlantDTO))]
        public async Task<IHttpActionResult> PostKlant(KlantDTO klantDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.Klant.Add(klant);
            //await db.SaveChangesAsync();
            Mapper.CreateMap<KlantDTO, Klant>();
            Klant klant = Mapper.Map<Klant>(klantDTO);
            db.Klant.Add(klant);
            await db.SaveChangesAsync();

            return Ok(klantDTO);
        }

        // DELETE: api/Klants/5
        [ResponseType(typeof(Klant))]
        public async Task<IHttpActionResult> DeleteKlant(short id)
        {
            Klant klant = await db.Klant.FindAsync(id);
            if (klant == null)
            {
                return NotFound();
            }

            db.Klant.Remove(klant);
            await db.SaveChangesAsync();

            return Ok(klant);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KlantExists(short id)
        {
            return db.Klant.Count(e => e.Klant_id == id) > 0;
        }
    }
}