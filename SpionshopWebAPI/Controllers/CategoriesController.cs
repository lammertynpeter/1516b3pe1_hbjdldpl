﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using SpionshopWebAPI.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SpionshopWebAPI.Controllers
{
    public class CategoriesController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Categories
        public IQueryable<CategorieDTO> GetCategorie()
        {
            Mapper.CreateMap<Categorie, CategorieDTO>();
            var cat = db.Categorie.
                   ProjectTo<CategorieDTO>();
            // return db.Categorie;
            var Categorie = from b in db.Categorie
                            select new CategorieDTO()
                            {
                                Cat_id = b.Cat_id,
                                Categorie1 = b.Categorie1,

                            };
            return Categorie;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> GetCategorie(short id)
        {
            Mapper.CreateMap<Categorie, CategorieDTO>();
            var cat = await db.Categorie.
                ProjectTo<CategorieDTO>().SingleOrDefaultAsync(b => b.Cat_id == id);
            if (cat == null)
            {
                return NotFound();
            }

            return Ok(cat);

        }

        public async Task<IHttpActionResult> GetCategorie(string naam)
        {
            Mapper.CreateMap<Categorie, CategorieDTO>();
            var cat = await db.Categorie.
                ProjectTo<CategorieDTO>().SingleOrDefaultAsync(b => b.Categorie1 == naam);
            if (cat == null)
            {
                return NotFound();
            }

            return Ok(cat);

        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategorie(short id, CategorieDTO categorieDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorieDTO.Cat_id)
            {
                return BadRequest();
            }
            Mapper.CreateMap<CategorieDTO, Categorie>();
            Categorie categorie = Mapper.Map<Categorie>(categorieDTO);

            db.Entry(categorie).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategorieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> PostCategorie(CategorieDTO categorieDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Mapper.CreateMap<CategorieDTO, Categorie>();
            Categorie categorie = Mapper.Map<Categorie>(categorieDTO);

            db.Categorie.Add(categorie);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = categorie.Cat_id }, categorie);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> DeleteCategorie(short id)
        {
            Categorie categorie = await db.Categorie.FindAsync(id);

            if (categorie == null)
            {
                return NotFound();
            }

            
            if (await db.BestellingDetail.AnyAsync(c => c.Artikel.Cat_id == id))
            {
                return StatusCode(HttpStatusCode.Conflict);
            }
            else
            {
                
                db.Categorie.Remove(categorie);
                await db.SaveChangesAsync();

                return Ok(categorie);
            }
           // return Ok(categorie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategorieExists(short id)
        {
            return db.Categorie.Count(e => e.Cat_id == id) > 0;
        }
    }
}