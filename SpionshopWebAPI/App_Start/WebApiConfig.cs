﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;



namespace SpionshopWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //name: "Artikels",
            //routeTemplate: "api/Artikels/{id}",
            //defaults: new { controller = "Artikels", id = RouteParameter.Optional }
            //);

            //config.Routes.MapHttpRoute(
            //name: "Bestellings",
            //routeTemplate: "api/Bestellings/{id}",
            //defaults: new { controller = "Bestellings", id = RouteParameter.Optional }
            //);

            //config.Routes.MapHttpRoute(
            //name: "BestellingDetails",
            //routeTemplate: "api/BestellingDetails/{id}",
            //defaults: new { controller = "BestellingDetails", id = RouteParameter.Optional }
            //);

            //config.Routes.MapHttpRoute(
            //name: "Categories",
            //routeTemplate: "api/Categories/{id}",
            //defaults: new { controller = "Categories", id = RouteParameter.Optional }
            //);

        }
    }
}
