﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SpionshopLib;
using SpionshopWebAPI.Models.Proxies;
using AutoMapper;
using SpionshopWebAPI.Models;

namespace SpionshopWebAPI
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Token, TokenDTO>();
            Mapper.CreateMap<Klant, KlantDTO>();
            Mapper.CreateMap<Artikel, ArtikelDetailDTO>();
            Mapper.CreateMap<Artikel, ArtikelDTO>();
            Mapper.CreateMap<Bestelling, BestellingDTO>();
            Mapper.CreateMap<BestellingDetail, BestellingDetailDTO>();
            Mapper.CreateMap<Categorie, CategorieDTO>();
        }
    }
}