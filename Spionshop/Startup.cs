﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Spionshop.Startup))]
namespace Spionshop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
