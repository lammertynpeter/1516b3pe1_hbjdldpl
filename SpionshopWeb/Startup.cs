﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpionshopWeb.Startup))]
namespace SpionshopWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
