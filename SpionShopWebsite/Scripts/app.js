﻿// starturi's voor Web API Services
var bestellingsUri = '/api/Bestellings/';
var bestellingDetailsUri = '/api/BestellingDetails?B_id=';




var BestellingenViewModel = function () {

    var self = this;
    self.error = ko.observable();
    self.bestellingen = ko.observableArray();
    self.bestellingDetails = ko.observableArray();

    self.Bestelling = function(b_id, klant_id, klantnaam, datum)
    {
        this.B_id = ko.observable(b_id),
        this.Klant_id = ko.observable(klant_id),
        this.KlantNaam = ko.observable(klantnaam)
        this.Datum = ko.observable(datum)
    }

    self.BestellingDetail = function (bd_id, aantal, artikel) {
        this.BD_id = ko.observable(bd_id),
        this.Aantal = ko.observable(aantal),
        this.Artikel = ko.observable(artikel)
    }


    //nog aanpassen
    self.isEdit = ko.observable(true);
    self.actionDescription = ko.computed(
        function () {
            if (self.isEdit()) return 'Edit';
            else return 'Add';
        }
        );


    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }

    self.getBestellingDetails = function (item1) {

        self.ajaxHelper(bestellingDetailsUri + item1.B_id(), 'GET')
            .done(function (data) {
                self.bestellingDetails(ko.utils.arrayMap(data, function (item) {
                    return new self.BestellingDetail(item.BD_id, item.Aantal, item.ArtikelArtikel1);
                }));
            })
    }
    self.getAllBestellingen = function () {

        self.ajaxHelper(bestellingsUri, 'GET')
           .done(function (data) {
               self.bestellingen(ko.utils.arrayMap(data, function (item) {
                   return new self.Bestelling(item.B_id, item.Klant_id, item.KlantNaam, item.Datum);
               }));
           })
    }

    // initiele data ophalen
    self.getAllBestellingen();

};

$(document).ready(function () {
    ko.applyBindings(new BestellingenViewModel());
});

//ko.applyBindings(new BestellingDetailsViewModel());