﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SpionshopWebAPI.Models;


namespace SpionShopWebsite.ViewModels
{
    public class SignInViewModel
    {
        [Required, Display(Name = "Gebruikersnaam")]
        public string UserName { get; set; }

        [Required, Display(Name = "Paswoord")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}