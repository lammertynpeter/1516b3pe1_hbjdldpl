﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SpionShopWebsite.Models;
using SpionshopLib;
using SpionshopWebAPI.Models.Proxies;
using System.ComponentModel.DataAnnotations;

namespace SpionShopWebsite.ViewModels
{
    public class WinkelMandViewModel
    {
        public List<BestellingDetailDTO> BesteldeArtikels { get; set; }
        public BestellingDetailDTO lijn {get; set;}
        public string artikel { get; set; }
        public short aantal { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? factuurPrijs { get; set; }
        public string Naam { get; set; }


    }

}