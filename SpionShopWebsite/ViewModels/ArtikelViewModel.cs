﻿using SpionshopLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpionShopWebsite.ViewModels
{
   public class ArtikelViewModel
    {
        public List<Artikel> artikel { get; set; }
        public string com { get; set; }
        public string afl { get; set; }
        public string alg { get; set; }
    }
}
