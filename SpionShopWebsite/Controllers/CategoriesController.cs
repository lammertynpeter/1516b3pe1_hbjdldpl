﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SpionshopLib;
using System.Net.Http;
using Newtonsoft.Json;
using SpionshopWebAPI.Models;
using AutoMapper;

namespace SpionShopWebsite.Controllers
{
    public class CategoriesController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: Categories
        public async Task<ActionResult> Index()
        {

            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/";
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<List<CategorieDTO>>(response.Result)
                            )
                            .Result
                         );
            }
        }

        // GET: Categories/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<CategorieDTO>(response.Result)
                            )
                            .Result
                         );
            }
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CategorieDTO categorieDTO)
        {
            if (ModelState.IsValid)
            {
         
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/";
                using (HttpClient httpClient = new HttpClient())
                {
                    HttpResponseMessage response =  await httpClient.PostAsJsonAsync(uri, categorieDTO);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }            
                }

            }

            return View(categorieDTO);
        }

        // GET: Categories/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
           if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<CategorieDTO>(response.Result)
                            )
                            .Result
                         );
            }
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CategorieDTO categorieDTO)
        {
            if (ModelState.IsValid)
            {
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/" + categorieDTO.Cat_id;
                using (HttpClient httpClient = new HttpClient())
                {
                    HttpResponseMessage response = await httpClient.PutAsJsonAsync(uri, categorieDTO);
                    if(response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }

                }
                ;
            }
            return View(categorieDTO);
        }

        // GET: Categories/Delete/5
        public async Task<ActionResult> Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<CategorieDTO>(response.Result)
                            )
                            .Result
                         );
            }
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/categories/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                HttpResponseMessage response = await httpClient.DeleteAsync(uri);

                return
                    RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}
