﻿using Newtonsoft.Json;
using SpionshopLib;
using SpionshopWebAPI.Models;
using SpionShopWebsite.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SpionShopWebsite.Controllers
{
    public class HomeController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();

        public ActionResult Index()
        {
            List<ArtikelDetailDTO> lijstArtikels = new List<ArtikelDetailDTO>();

            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/artikels/";
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                lijstArtikels = Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<List<ArtikelDetailDTO>>(response.Result)
                            )
                            .Result;  
            }
           
            //groeperen per categorie om door elke categorie te kunnen loopen
            var groupModel = lijstArtikels.GroupBy(item => item.CategorieCategorie1).ToArray();
            //lijst die random artikel uit elke categorie zal bevatten
            List<ArtikelDetailDTO> lijstRandom = new List<ArtikelDetailDTO>();
            int count = 0;
            // per categorie een random artikel selecteren
            foreach (var group in groupModel)
            {
                count = group.Count();
                Random rnd = new Random();
                var grouplijst = lijstArtikels.Where(item => item.CategorieCategorie1 == group.Key).ToArray();
                lijstRandom.Add(grouplijst[rnd.Next(0, count)]);

            }

            return View(lijstRandom);

        }
    }
}
