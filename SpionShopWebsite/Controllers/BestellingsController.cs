﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SpionshopLib;
using System.Net.Http;
using Newtonsoft.Json;
using SpionshopWebAPI.Models.Proxies;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SpionShopWebsite.Controllers
{
    public class BestellingsController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();

        //GET: Bestellings
        

        public async Task<ActionResult> Index()
        {
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/bestellings/";
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<List<BestellingDTO>>(response.Result)
                            )
                            .Result
                         );
            }
        }

        ////GET: Bestellings/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/bestellings/" + id;
        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        Task<String> response = httpClient.GetStringAsync(uri);

        //        return
        //            View(
        //                    Task.Factory.StartNew
        //                    (
        //                        () => JsonConvert
        //                                .DeserializeObject<BestellingDTO>(response.Result)
        //                    )
        //                    .Result
        //                 );
        //    }

        //}

        // GET: Bestellings/Create
        public ActionResult Create()
        {
            ViewBag.Klant_id = new SelectList(db.Klant, "Klant_id", "Naam");
            return View();
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BestellingDTO bestellingDTO)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<BestellingDTO, Bestelling>();
                Bestelling bestelling = Mapper.Map<Bestelling>(bestellingDTO);

                db.Bestelling.Add(bestelling);
                await db.SaveChangesAsync();
            }

            return View(bestellingDTO);
        }

        // GET: Bestellings/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return HttpNotFound();
            }
            ViewBag.Klant_id = new SelectList(db.Klant, "Klant_id", "Naam", bestelling.Klant_id);
            return View(bestelling);


        }





//        // POST: Bestellings/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public async Task<ActionResult> Edit([Bind(Include = "B_id,Klant_id,Datum")] Bestelling bestelling)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(bestelling).State = EntityState.Modified;
//                await db.SaveChangesAsync();
//                return RedirectToAction("Index");
//            }
//            ViewBag.Klant_id = new SelectList(db.Klant, "Klant_id", "Naam", bestelling.Klant_id);
//            return View(bestelling);
//        }

//        // GET: Bestellings/Delete/5
//        public async Task<ActionResult> Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Bestelling bestelling = await db.Bestelling.FindAsync(id);
//            if (bestelling == null)
//            {
//                return HttpNotFound();
//            }
//            return View(bestelling);
//        }

//        // POST: Bestellings/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public async Task<ActionResult> DeleteConfirmed(int id)
//        {
//            Bestelling bestelling = await db.Bestelling.FindAsync(id);
//            db.Bestelling.Remove(bestelling);
//            await db.SaveChangesAsync();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
}
}
