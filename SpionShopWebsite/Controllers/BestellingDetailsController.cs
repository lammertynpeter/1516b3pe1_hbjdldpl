﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SpionshopLib;
using SpionshopWebAPI.Models.Proxies;
using AutoMapper;
using System.Net.Http;

namespace SpionShopWebsite.Controllers
{
    public class BestellingDetailsController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();

        //// GET: BestellingDetails
        //public async Task<ActionResult> Index()
        //{
        //    var bestellingDetail = db.BestellingDetail.Include(b => b.Artikel).Include(b => b.Bestelling);
        //    return View(await bestellingDetail.ToListAsync());
        //}

        //// GET: BestellingDetails/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
        //    if (bestellingDetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bestellingDetail);
        //}

        //// GET: BestellingDetails/Create
        //public ActionResult Create()
        //{
        //    ViewBag.Artikel_id = new SelectList(db.Artikel, "Artikel_id", "Artikel1");
        //    ViewBag.B_id = new SelectList(db.Bestelling, "B_id", "B_id");
        //    return View();
        //}

        // POST: BestellingDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BestellingDetailDTO bestellingDetailDTO)
        {
            if (ModelState.IsValid)
            {
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/bestellingDetails/";
                using (HttpClient httpClient = new HttpClient())
                {
                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(uri, bestellingDetailDTO);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }

                }
                ;
            }

            ViewBag.Artikel_id = new SelectList(db.Artikel, "Artikel_id", "Artikel1", bestellingDetailDTO.Artikel_id);
            ViewBag.B_id = new SelectList(db.Bestelling, "B_id", "B_id", bestellingDetailDTO.B_id);
            return View(bestellingDetailDTO);
        }

        //// GET: BestellingDetails/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
        //    if (bestellingDetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.Artikel_id = new SelectList(db.Artikel, "Artikel_id", "Artikel1", bestellingDetail.Artikel_id);
        //    ViewBag.B_id = new SelectList(db.Bestelling, "B_id", "B_id", bestellingDetail.B_id);
        //    return View(bestellingDetail);
        //}

        //// POST: BestellingDetails/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "BD_id,B_id,Artikel_id,Aantal")] BestellingDetail bestellingDetail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(bestellingDetail).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Artikel_id = new SelectList(db.Artikel, "Artikel_id", "Artikel1", bestellingDetail.Artikel_id);
        //    ViewBag.B_id = new SelectList(db.Bestelling, "B_id", "B_id", bestellingDetail.B_id);
        //    return View(bestellingDetail);
        //}

        //// GET: BestellingDetails/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
        //    if (bestellingDetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bestellingDetail);
        //}

        //// POST: BestellingDetails/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
        //    db.BestellingDetail.Remove(bestellingDetail);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
