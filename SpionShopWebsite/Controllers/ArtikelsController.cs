﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SpionshopLib;
using SpionShopWebsite.Models;
using SpionShopWebsite.Areas.Admin.Controllers;
using System.Net.Http;
using Newtonsoft.Json;
using SpionshopWebAPI.Models;
using SpionshopWebAPI.Models.Proxies;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SpionShopWebsite
{
    public class ArtikelsController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();
        private decimal? totaal;

        // GET: Artikels
        public async Task<ActionResult> Index()
        {
            //WinkelWagen ww = new WinkelWagen();
            //// ww.GetCart(this,ww);
            //ww.GetCart(this);

            //var artikel = db.Artikel.Include(a => a.Categorie);
            //return View(await artikel.ToListAsync());

            string uri = "http://" + Request.Url.Host + ':' + Request.Url.Port + "/api/artikels";
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<List<ArtikelDTO>>(response.Result)
                            )
                            .Result
                         );
            }
        }

        // GET: Artikels/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string uri = "http://" + Request.Url.Host + ':' + Request.Url.Port + "/api/artikels/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<ArtikelDetailDTO>(response.Result)
                            )
                            .Result
                         );
            }
        
    }

        // GET: Artikels/Create
        public ActionResult Create()
        {
            ViewBag.Cat_id = new SelectList(db.Categorie, "Cat_id", "Categorie1");
            return View();
        }

        // POST: Artikels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Artikel_id,Cat_id,Artikel1,Omschrijving,Verkoopprijs,Instock")] ArtikelDTO artikelDTO)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<ArtikelDTO, Artikel>();
                Artikel artikel = Mapper.Map<Artikel>(artikelDTO);

                db.Artikel.Add(artikel);
                await db.SaveChangesAsync(); 
                return RedirectToAction("Index");
            }

            ViewBag.Cat_id = new SelectList(db.Categorie, "Cat_id", "Categorie1", artikelDTO.Artikel_id);
            return View(artikelDTO);
        }

        // GET: Artikels/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artikel artikel = await db.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cat_id = new SelectList(db.Categorie, "Cat_id", "Categorie1", artikel.Cat_id);
            return View(artikel);
        }

        // POST: Artikels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Artikel_id,Cat_id,Artikel1,Omschrijving,Verkoopprijs,Instock")] Artikel artikel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artikel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Cat_id = new SelectList(db.Categorie, "Cat_id", "Categorie1", artikel.Cat_id);
            return View(artikel);
        }

        // GET: Artikels/Delete/5
        public async Task<ActionResult> Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artikel artikel = await db.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return HttpNotFound();
            }
            return View(artikel);
        }

        // POST: Artikels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(short id)
        {
            Artikel artikel = await db.Artikel.FindAsync(id);
            db.Artikel.Remove(artikel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Artikels

        public async Task<ActionResult> Artikel(String naam)
        {

            UriBuilder baseUri = new UriBuilder("http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/artikels?");
            string queryToAppend = naam;

            if (baseUri.Query != null)
                baseUri.Query = baseUri.Query.Substring(1) + queryToAppend;
            string uri = baseUri.ToString();

            using (HttpClient httpClient = new HttpClient())
            {

                Task<String> response = httpClient.GetStringAsync(uri);

                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<List<ArtikelDetailDTO>>(response.Result)
                            )
                            .Result
                         );
            }

        }

        public async Task<ActionResult> ArtikelPerCategorie(String naam)
        {
           
            
            UriBuilder baseUri = new UriBuilder("http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/artikels?");
            string queryToAppend = "categorienaam=" + naam;
            if (baseUri.Query != null)
                baseUri.Query = baseUri.Query.Substring(1) + queryToAppend;
            string uri = baseUri.ToString();
           
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                return
                    View(
                        Task.Factory.StartNew
                        (
                            () => JsonConvert
                                .DeserializeObject<List<ArtikelDetailDTO>>(response.Result)
                         )
                            .Result
                         );
                 }
            }



        public async Task<ActionResult> Detail(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var artikel = db.Artikel.Where(c => c.Artikel_id == id);
            return View(await artikel.ToListAsync());
        }
      
        public JsonResult GetCategories()
        {
            List<String> Categories = new List<string>();
            List<Categorie> categories = db.Categorie.ToList();
            foreach (Categorie c in categories) {
                Categories.Add(c.Categorie1);
                    }
       
            return Json(new { lcat=Categories });
        }

        
        [HttpPost]
        public ActionResult UpdateWinkelMand(string textBox1,string name,bool aanpassing)
        //public String UpdateWinkelMand(string textBox1, string name, bool aanpassing)
        {
            
            short id = Int16.Parse(name);
            Artikel artikel = db.Artikel.Find(id);

            //checken of er al een winkelmand bestaat voor huidige sessie, indien niet maak er een aan
            WinkelWagen ww = new WinkelWagen();
               ww= ww.GetCart(this, ww);

           // voeg artikel toe aan winkelmand of pas hoeveelheid aan
            if (aanpassing)
            {
                ww.PasBestelingAan(artikel, short.Parse(textBox1), ww);
            }

            else { 
           ww.AddToCart(artikel, short.Parse(textBox1),ww);
           TempData["succesboodschap"] = "Winkelmand gevuld met " +textBox1+ " " + artikel.Artikel1;
                    }
            return JavaScript("document.location.replace('" + Url.Action("Index") + "');");

        }

        public JsonResult UpdateWinkelMandje(BestellingDetailDTO lijn)
        {
          
            Artikel artikel = db.Artikel.Find(lijn.Artikel_id);
            WinkelWagen ww = new WinkelWagen();
            ww = ww.GetCart(this, ww);
            ww.PasBestelingAan(artikel,lijn.Aantal, ww);
            return Json(new  { aantal = lijn.Aantal, totaal=artikel.Verkoopprijs*lijn.Aantal,benaming=artikel.Artikel1});
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
