﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using SpionShopWebsite.ViewModels;

namespace SpionShopWebsite.Controllers
{
    public class AccountController : Controller
    {

        // GET: Account/SignIn
        public ActionResult SignIn()
        {
            return View();
        }

        // GET: Account/SignIn
        //public ActionResult SignIn(string redirectUrl)
        //{
        //    //So that the user can be referred back to where they were when they click logon
        //    if (string.IsNullOrEmpty(redirectUrl) && Request.UrlReferrer != null)
        //        redirectUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);

        //    if (Url.IsLocalUrl(redirectUrl) && !string.IsNullOrEmpty(redirectUrl))
        //    {
        //        ViewBag.ReturnURL = redirectUrl;
        //    }
        //    return View();
        //}

        // POST: Account/SignIn
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn(SignInViewModel model, string redirectUrl)
        {
            if (ModelState.IsValid)
            {
                //returnURL needs to be decoded
                string decodedUrl = "";
                if (!string.IsNullOrEmpty(redirectUrl))
                    decodedUrl = Server.UrlDecode(redirectUrl);

                //var result = await WebApiService.Instance.AuthenticateAsync<SignInResult>(model.Email, model.Password);
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/Authenticate/";

                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue(
                            "Basic",
                            Convert.ToBase64String(Encoding.ASCII.GetBytes(
                                    string.Format("{0}:{1}", model.UserName, model.Password))));
                    StringContent content = new StringContent("");
                    HttpResponseMessage response = await httpClient.PostAsync(uri, content);
                    if (response.IsSuccessStatusCode)
                    {
                        var token = response.Headers.GetValues("Token").FirstOrDefault();
                        Session["Token"] = token;
                        Session["UserName"] = model.UserName;
                        //FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie:true);

                        if (Url.IsLocalUrl(decodedUrl))
                        {
                            return Redirect(decodedUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "The username and password provided do not match any accounts on record.");
                        return View(model);
                    }

                }
            }
            return View(model);



        }

        public async Task<ActionResult> LogOut()
        {
            Session.Remove("Token");
            Session.Remove("UserName");
            return Redirect("~/");
        }
    }
}