﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Newtonsoft.Json;
using SpionshopLib;
using SpionshopWebAPI.Models;

namespace SpionShopWebsite.Controllers
{
    public class KlantsController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: Klants
        public async Task<ActionResult> Index()
        {
            return View(await db.Klant.ToListAsync());
        }

        // GET: Klants/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klant klant = await db.Klant.FindAsync(id);
            if (klant == null)
            {
                return HttpNotFound();
            }
            return View(klant);
        }

        // GET: Klants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Klants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(KlantDTO klantDTO)
        {
            //if (ModelState.IsValid)
            //{
            //    Mapper.CreateMap<KlantDTO, Klant>();
            //    Klant klant = 
            //    db.Klant.Add(klantDTO);
            //    await db.SaveChangesAsync();
            //    return RedirectToAction("Index");
            //}

            //return View(klantDTO);
            if (ModelState.IsValid)
            {
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/klants/";
                using (HttpClient httpClient = new HttpClient())
                {
                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(uri, klantDTO);
                    if (response.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        return RedirectToAction("Bedankt");
                    }
                }
            }
            return View(klantDTO);
        }

        // GET: Winkelmand/Bevestigbestelling
        public ActionResult Bedankt()
        {
            return View();
        }


        // GET: Klants/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/klants/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                return
                    View(
                            Task.Factory.StartNew
                            (
                                () => JsonConvert
                                        .DeserializeObject<KlantDTO>(response.Result)
                            )
                            .Result
                         );
            }
            //Klant klant = await db.Klant.FindAsync(id);
            //if (klant == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(klant);
        }

        // POST: Klants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Klant_id,Naam,Voornaam,Woonplaats,Geboortedatum,Gebruikersnaam,Pwd")] KlantDTO klantDTO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(klantDTO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(klantDTO);
        }

        // GET: Klants/Delete/5
        public async Task<ActionResult> Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klant klant = await db.Klant.FindAsync(id);
            if (klant == null)
            {
                return HttpNotFound();
            }
            return View(klant);
        }

        // POST: Klants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(short id)
        {
            Klant klant = await db.Klant.FindAsync(id);
            db.Klant.Remove(klant);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
