﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using SpionShopWebsite.Models;
using SpionShopWebsite.ViewModels;
using SpionshopLib;
using SpionshopWebAPI.Models.Proxies;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using SpionshopWebAPI.Models;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;

namespace SpionShopWebsite.Controllers
{
    public class WinkelmandController : Controller
    {
        private SpionshopEntities db = new SpionshopEntities();
        bool bestellingMogelijk = true;
        // GET: Winkelmand
        public ActionResult Index()
        {              
            return View(MaakViewModel());
            }

        private WinkelMandViewModel MaakViewModel()
        {
            WinkelWagen ww = new WinkelWagen();
            var cart = ww.GetCart(this.HttpContext);
            List<BestellingDetailDTO> lijnen = new List<BestellingDetailDTO>();
            lijnen = ww.GetLijnen(cart.WinkelWagenId);
            lijnen = ww.CheckZelfdeProduct();
            WinkelMandViewModel viewModel = new WinkelMandViewModel();
            viewModel.BesteldeArtikels = lijnen;
            viewModel.factuurPrijs = berekenTotaal(lijnen);
            return viewModel;
        }

        private decimal? berekenTotaal(List<BestellingDetailDTO> lijnen)
        {
            decimal? totaal = 0;
            foreach (BestellingDetailDTO l in lijnen)
            {
                totaal += l.TotaalPrijs;
            }
            return totaal;
        }

        [HttpPost]
        public String UpdateWinkelMand(string textBox1, string name, bool aanpassing)
        {
            short id = Int16.Parse(name);

            ArtikelDetailDTO artikel = new ArtikelDetailDTO();

            string uri = "http://" + Request.Url.Host + ':' + Request.Url.Port + "/api/artikels/" + id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                artikel = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ArtikelDetailDTO>(response.Result)).Result;


            }
            //checken of er al een winkelmand bestaat voor huidige sessie, indien niet maak er een aan
            WinkelWagen ww = new WinkelWagen();
            ww = ww.GetCart(this, ww);
            ww.AddToCartDTO(artikel, short.Parse(textBox1), ww);

            return "<h2>Winkelmand gevuld met " + textBox1 + " " + artikel.Artikel1 + " </h2>";


        }

        public JsonResult UpdateWinkelMandje(BestellingDetailDTO lijn)
        {

            WinkelWagen ww = new WinkelWagen();
            ww = ww.GetCart(this, ww);

            ww.PasBestelingDTOAan(lijn.Artikel_id, lijn.Aantal, ww);
            //WinkelMandViewModel wm = MaakViewModel();
            WinkelMandViewModel wm = new WinkelMandViewModel();
            wm = MaakViewModel();

            string uri = "http://" + Request.Url.Host + ':' + Request.Url.Port + "/api/artikels/" + lijn.Artikel_id;
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                ArtikelDetailDTO result = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ArtikelDetailDTO>(response.Result)).Result;

                return Json(new { aantal = lijn.Aantal, totaal = result.Verkoopprijs * lijn.Aantal, benaming = result.Artikel1, factuurprijs = wm.factuurPrijs });
            }



        }


        [HttpPost]
        public async Task<ActionResult> MaakBestelling(WinkelMandViewModel vm)
        {
            WinkelMandViewModel bestellingsmodel = new WinkelMandViewModel();
            bestellingsmodel = vm;

            // Sessie variable token in deze methode krijgen: http://www.dotnetodyssey.com/2015/01/05/getset-value-session-variable-using-javascriptjquery/
            HttpContext context = System.Web.HttpContext.Current;
            if (ModelState.IsValid)
            {

                if (context.Session["Token"] == null || (String)context.Session["Token"] == "")
                {
                    return RedirectToAction("Signin", "Account");
                }
                else
                {
                    string apiUrl = "http://localhost:49497/api";
                    string uriDetail = apiUrl + "/bestellingDetails/";
                    string uri = apiUrl + "/bestellings/";
                    string getKlanturi = apiUrl + "/Klants/";
                    string userName = (string)context.Session["UserName"];

                    //check op in stock of niet
                    using (HttpClient httpClient = new HttpClient())
                    {
                        BestellingDTO nieuweBestellingDTO = new BestellingDTO
                        {
                            Datum = DateTime.Today
                        };
                        //nieuweBestellingDTO.Klant_id = 1;
                        //foutief: ophalen rechtstreeks op databank
                        //var firstOrDefault = db.Klant.FirstOrDefault(k => k.Gebruikersnaam == userName);
                        // klantnummer ophalen via api
                        Task<String> responseKlant = httpClient.GetStringAsync(getKlanturi + userName);
                        var resultKlant = Task.Factory.StartNew(() => JsonConvert
                            .DeserializeObject<KlantDTO>(responseKlant.Result)).Result;
                        if (resultKlant != null)
                        {
                            nieuweBestellingDTO.Klant_id = resultKlant.Klant_id;
                        } else {
                            nieuweBestellingDTO.Klant_id = 1;
                        }
                            

                        HttpResponseMessage response = await httpClient.PostAsJsonAsync(uri, nieuweBestellingDTO);
                        int nieuweBestellingDTOId = int.Parse(response.Headers.Location.Segments.Last());


                        foreach (BestellingDetailDTO bestellingDetailDTO in vm.BesteldeArtikels)
                        {
                            bestellingDetailDTO.B_id = nieuweBestellingDTOId;
                            HttpClient client = GetClient();
                            var url = "api/Artikels/" + bestellingDetailDTO.Artikel_id;
                            var respons = await client.GetAsync(url);
                            string arts = respons.Content.ReadAsStringAsync().Result;
                            JavaScriptSerializer JS = new JavaScriptSerializer();
                            ArtikelDetailDTO c = JS.Deserialize<ArtikelDetailDTO>(arts);
                            
                            if (bestellingDetailDTO.Aantal <= c.Instock)
                            {
                                c.Instock -= bestellingDetailDTO.Aantal;
                                var res = await client.PutAsJsonAsync(url, c);
                            }
                            else
                            {
                                bestellingMogelijk = false;
                            
                            }
                        }
                        if (bestellingMogelijk)
                        {
                            HttpResponseMessage responseDetail = await httpClient.PostAsJsonAsync(uriDetail, vm.BesteldeArtikels);

                            if (response.IsSuccessStatusCode)
                            {
                                var ww = new WinkelWagen();
                                var cart = ww.GetCart(this.HttpContext);
                                cart.EmptyCart();

                                vm.factuurPrijs = berekenTotaal(vm.BesteldeArtikels);
                                TempData["vm"] = vm;
                                vm.Naam = userName;
                                return RedirectToAction(actionName: "BevestigBestelling", controllerName: "Winkelmand", routeValues: new { BestellingId = nieuweBestellingDTOId });


                            }
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Index");
                //nog foutboodschap toevoegen
            }

            return RedirectToAction("Index");

        }

        public HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:49497/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }


        // GET: Winkelmand/Bevestigbestelling
        public ActionResult BevestigBestelling(int BestellingId)
        {
            if (BestellingId == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //nog clearen na gebruik
        
            WinkelMandViewModel vm = (WinkelMandViewModel)TempData["vm"];
            
            return View(vm);
        }

        // GET: Winkelmand/Edit/5
        [HttpPost]
        public PartialViewResult Edit(BestellingDetailDTO l)
        {
            return PartialView(l);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BestellingDetailDTO bestellingDetailDTO)
        {
            if (ModelState.IsValid)
            {
                string uri = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/api/bestellingDetails/";
                using (HttpClient httpClient = new HttpClient())
                {
                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(uri, bestellingDetailDTO);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }

                }
                ;
            }
            return null;
        }


        [HttpPost]
        public JsonResult Delete(BestellingDetailDTO lijn)
        {
            WinkelWagen ww = new WinkelWagen();
            ww = ww.GetCart(this, ww);

            string boodschap = "Artikel " + lijn.ArtikelArtikel1 + " werd verwijderd";
            ww.VerwijderBestelling(lijn, ww);
           WinkelMandViewModel wm= MaakViewModel();

            return Json(new { lijn, boodschap= "<h2>"+boodschap+"</h2>",factuurprijs=wm.factuurPrijs});
        }

    }
}
