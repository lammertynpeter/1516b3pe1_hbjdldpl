﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Web.Compilation;
using System.IO;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Data;
using System.Web.Mvc;

namespace SpionShopWebsite
{
    public static class ImageHTMLHelper
    {
        public static string ImageUrlFor(this HtmlHelper helper, string contentUrl)
        {
            // Put some caching logic here if you want it to perform better
            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            if (!File.Exists(helper.ViewContext.HttpContext.Server.MapPath(contentUrl)))
            {
                return urlHelper.Content("~/Content/Images/notfound.gif");
            }
            else
            {
                return urlHelper.Content(contentUrl);
            }
        }
    }
}