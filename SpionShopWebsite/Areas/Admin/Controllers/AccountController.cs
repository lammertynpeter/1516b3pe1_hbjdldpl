﻿using SpionShopWebsite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SpionShopWebsite.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            SignInViewModel model = new SignInViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(SignInViewModel model)
        {
            //SpionshopWebAPI.Controllers.AccountController test = new SpionshopWebAPI.Controllers.AccountController();
            //var log =test.GetUserInfo();
            if (ModelState.IsValid)
            {
                if (model.Password == "123" && model.UserName.ToUpper() == "ADMIN") //geldige credentials
                //OPMERKING:    deze credentials komen normaal gezien uit de database. Hier hoort dus eigenlijk een
                //              Query te staan ipv hardgecodeerde credentials!
                {
                    //inloggen door een authenticatie cookie te plaatsen op client
                    FormsAuthentication.SetAuthCookie("ADMIN", model.RememberMe);
                    //redirect naar homepage
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                else
                { //ongeldige gebruikersnaam of wachtwoord
                    //voeg een foutboodschap toe (wanneer key == "", dan hoort deze niet bij een bepaalde property)
                    ModelState.AddModelError("", "De ingevoerde gebruikersnaam of wachtwoord is ongeldig");
                    //aanmeldingscherm opnieuw tonen
                    return View(model);
                }

            }
            else
            {
                //onvolledige gegevens, toon formulier opnieuw
                return View(model);
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}