﻿using SpionshopLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpionshopWebAPI.Models.Proxies;
using SpionshopWebAPI.Models;

namespace SpionShopWebsite.Models
{

    public partial class WinkelWagen
    {

        SpionshopEntities db = new SpionshopEntities();
        public string WinkelWagenId { get; set; }

        
        public static List<BestellingDetailDTO> Lijnen = new List<BestellingDetailDTO>();
        public const string CartSessionKey = "CartId";

        public  WinkelWagen GetCart(HttpContextBase context,WinkelWagen ww)
        {
            //bepalen welk ww. -> versch. gebruikers op site
           
           ww.WinkelWagenId = ww.GetCartId(context);
            return ww;
        }

        public List<BestellingDetailDTO> CheckZelfdeProduct()
        {
            for (int x = 0; x < Lijnen.Count - 1; x++) {
                for (int i = x+1; i < Lijnen.Count ; i++)
                {
                    var a = Lijnen[x];
                    var b = Lijnen[i ];
                    if (a.Artikel_id == b.Artikel_id)
                    {

                        a.Aantal += b.Aantal;
                        Lijnen.RemoveAt(i);
                    }
                }
               
            }
            BerekenPrijs();
            return Lijnen; 

        }

        private void BerekenPrijs()
        {
            foreach (BestellingDetailDTO l in Lijnen)
            {
                Artikel arti;
                arti = db.Artikel.FirstOrDefault(p => p.Artikel_id == l.Artikel_id);
                if (arti != null) l.TotaalPrijs = arti.Verkoopprijs * l.Aantal;
            }
        }

        public List<BestellingDetailDTO> GetLijnen(string winkelWagenId)
        {
            List<BestellingDetailDTO> l;
            l = Lijnen.Where(w => w.KarId == winkelWagenId).ToList();
            return l;
        }

        public WinkelWagen GetCart(HttpContextBase context)
        {
            WinkelWagen ww = new WinkelWagen();
            ww.WinkelWagenId = ww.GetCartId(context);
            
            return ww;
        }
        // Helper method to simplify shopping cart calls
        public  WinkelWagen GetCart(Controller controller,WinkelWagen ww)
        {
            return GetCart(controller.HttpContext,ww);
        }

        public WinkelWagen GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        public void AddToCart(Artikel artikel,short aantal,WinkelWagen ww)
        {
            BestellingDetailDTO lijn = new BestellingDetailDTO
            {
                Artikel_id = artikel.Artikel_id,
                KarId = ww.WinkelWagenId,
                Aantal = aantal,
                ArtikelArtikel1 = artikel.Artikel1,
                ArtikelVerkoopprijs = artikel.Verkoopprijs
                    
                };
            Lijnen.Add(lijn);

        }

        public void AddToCartDTO(ArtikelDetailDTO artikel, short aantal, WinkelWagen ww)
        {
            BestellingDetailDTO lijn = new BestellingDetailDTO
            {
                Artikel_id = artikel.Artikel_id,
                KarId = ww.WinkelWagenId,
                Aantal = aantal,
                ArtikelArtikel1 = artikel.Artikel1,
                ArtikelVerkoopprijs = artikel.Verkoopprijs

            };
            Lijnen.Add(lijn);

        }

        internal void PasBestelingAan(Artikel artikel, short aantal, WinkelWagen ww)
        {
            string id = ww.WinkelWagenId;
            var bestelling = ww.GetLijnen(id);
            bestelling = bestelling.Where(art => art.Artikel_id == artikel.Artikel_id).ToList();
            
            foreach (BestellingDetailDTO l in bestelling)
            {
                if (l.Artikel_id == artikel.Artikel_id)
                {
                    l.Aantal = aantal;
                    l.TotaalPrijs = artikel.Verkoopprijs * aantal;
                }
            }
        }

        internal void PasBestelingDTOAan(short artikelId, short aantal, WinkelWagen ww)
        {
            string id = ww.WinkelWagenId;
            var bestelling = ww.GetLijnen(id);
            bestelling = bestelling.Where(art => art.Artikel_id == artikelId).ToList();

            foreach (BestellingDetailDTO l in bestelling)
            {
                if (l.Artikel_id == artikelId)
                {
                    l.Aantal = aantal;
                    l.TotaalPrijs = l.ArtikelVerkoopprijs * aantal;
                }
            }
        }


        internal void VerwijderBestelling(BestellingDetailDTO artikel, WinkelWagen ww)
        {
            Lijnen.RemoveAll(a => (a.KarId == ww.WinkelWagenId) && (a.Artikel_id == artikel.Artikel_id));
        }

        public void EmptyCart()
        {
            Lijnen.Clear();
        }

        private void SetLijnen(BestellingDetailDTO bestelling)
        {
            Lijnen.Add(bestelling);
        }

        //        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session != null && context.Session[CartSessionKey] == null)
                    {
                        if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                        {
                            context.Session[CartSessionKey] =
                                context.User.Identity.Name;
                        }
                        else
                        {
                            // Generate a new random GUID using System.Guid class
                            Guid tempCartId = Guid.NewGuid();
                            // Send tempCartId back to client as a cookie
                            context.Session[CartSessionKey] = tempCartId.ToString();
                        }
                    }
                    return context.Session[CartSessionKey].ToString();
                }

    }
}

