﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using SpionshopLib;

namespace SpionShopWebsite.Models
{
   
        public class Lijn
        {
            [Key]
            public int RecordId { get; set; }
            public string KarId { get; set; }
            public int ArtikelId { get; set; }
            public int Aantal { get; set; }
            public decimal? TotaalPrijs { get; set; }
            public string Benaming { get; set; }
            public System.DateTime DateCreated { get; set; }
            public virtual Artikel Artikel { get; set; }
        }
    
}
